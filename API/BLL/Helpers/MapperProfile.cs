﻿using DAL.Models;
using AutoMapper;
using DTO;

namespace BLL.Helpers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<Task, TaskDTO>().ReverseMap();
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<TaskState, TaskStateDTO>().ReverseMap();
        }
    }
}
