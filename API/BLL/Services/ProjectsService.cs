﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public ProjectsService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(ProjectDTO item)
        {
            var mapped = _mapper.Map<Project>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.Projects.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var status = _unitOfWork.Projects.Delete(id);

            _unitOfWork.SaveChanges();

            return status;
        }

        public ProjectDTO Get(int id)
        {
            var project = _unitOfWork.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            var projects = _unitOfWork.Projects.GetAll();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public bool Update(ProjectDTO item)
        {
            var status = false;

            var mapped = _mapper.Map<Project>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Projects.Update(mapped);

                _unitOfWork.SaveChanges();
            }

            return status;
        }
    }
}
