﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TaskStatesService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TaskStateDTO item)
        {
            var mapped = _mapper.Map<TaskState>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.States.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var status = _unitOfWork.States.Delete(id);

            _unitOfWork.SaveChanges();

            return status;
        }

        public TaskStateDTO Get(int id)
        {
            var state = _unitOfWork.States.Get(id);

            return _mapper.Map<TaskStateDTO>(state);
        }

        public IEnumerable<TaskStateDTO> GetAll()
        {
            var states = _unitOfWork.States.GetAll();

            return _mapper.Map<IEnumerable<TaskStateDTO>>(states);
        }

        public bool Update(TaskStateDTO item)
        {
            var status = false;

            var mapped = _mapper.Map<TaskState>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.States.Update(mapped);

                _unitOfWork.SaveChanges();
            }

            return status;
        }
    }
}
