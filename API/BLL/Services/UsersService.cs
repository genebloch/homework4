﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(UserDTO item)
        {
            var mapped = _mapper.Map<User>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.Users.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var status = _unitOfWork.Users.Delete(id);

            _unitOfWork.SaveChanges();

            return status;
        }

        public UserDTO Get(int id)
        {
            var user = _unitOfWork.Users.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var users = _unitOfWork.Users.GetAll();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public bool Update(UserDTO item)
        {
            var status = false;

            var mapped = _mapper.Map<User>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Users.Update(mapped);

                _unitOfWork.SaveChanges();
            }

            return status;
        }
    }
}
