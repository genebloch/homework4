﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TeamsService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TeamDTO item)
        {
            var mapped = _mapper.Map<Team>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.Teams.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var status = _unitOfWork.Teams.Delete(id);

            _unitOfWork.SaveChanges();

            return status;
        }

        public TeamDTO Get(int id)
        {
            var team = _unitOfWork.Teams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            var teams = _unitOfWork.Teams.GetAll();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public bool Update(TeamDTO item)
        {
            var status = false;

            var mapped = _mapper.Map<Team>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Teams.Update(mapped);

                _unitOfWork.SaveChanges();
            }

            return status;
        }
    }
}
