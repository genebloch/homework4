﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TaskDTO> _tasksService;

        public TasksController(IService<TaskDTO> tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var tasks = _tasksService.GetAll();

            return Ok(tasks);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var task = _tasksService.Get(id);

            if (task == null) return NotFound(task);

            return Ok(task);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskDTO task)
        {
            var created = _tasksService.Create(task);

            if (!created) return BadRequest(task);

            return Created($"{task.Id}", task);
        }

        [HttpPut]
        public IActionResult Update([FromBody] TaskDTO task)
        {
            var updated = _tasksService.Update(task);

            if (!updated) return BadRequest(task);

            return Ok(task);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var deleted = _tasksService.Delete(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
