﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _teamsService;

        public TeamsController(IService<TeamDTO> teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var teams = _teamsService.GetAll();

            return Ok(teams);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var team = _teamsService.Get(id);

            if (team == null) return NotFound(team);

            return Ok(team);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TeamDTO team)
        {
            var created = _teamsService.Create(team);

            if (!created) return BadRequest(team);

            return Created($"{team.Id}", team);
        }

        [HttpPut]
        public IActionResult Update([FromBody] TeamDTO team)
        {
            var updated = _teamsService.Update(team);

            if (!updated) return BadRequest(team);

            return Ok(team);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var deleted = _teamsService.Delete(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
