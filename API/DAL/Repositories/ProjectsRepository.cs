﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class ProjectsRepository : IRepository<Project>
    {
        private readonly DataContext _context;

        public ProjectsRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(Project item) => _context.Projects.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Projects.FirstOrDefault(p => p.Id == id);

            if (item == null) return false;

            _context.Projects.Remove(item);
            return true;
        }

        public Project Get(int id) => _context.Projects.FirstOrDefault(u => u.Id == id);
        public IEnumerable<Project> GetAll() => _context.Projects;

        public bool Update(Project item)
        {
            var exists = _context.Projects.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
