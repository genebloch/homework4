﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Task
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MinLength(15)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        [Required]
        public int State { get; set; }
        [Range(1, int.MaxValue)]
        public int ProjectId { get; set; }
        [Range(1, int.MaxValue)]
        public int PerformerId { get; set; }
    }
}
