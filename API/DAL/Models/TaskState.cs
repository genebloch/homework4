﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Value { get; set; }
    }
}
