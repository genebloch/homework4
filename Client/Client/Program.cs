﻿using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static async Task Main()
        {
            await new Menu().RunAsync();
        }
    }
}
